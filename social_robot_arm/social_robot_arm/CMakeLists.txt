cmake_minimum_required(VERSION 2.8.3)
project(social_robot_arm)
find_package(catkin REQUIRED)
catkin_metapackage()
