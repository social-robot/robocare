#!/usr/bin/env python
import rospy
import rospkg
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry
from social_robot_arm_msgs.msg import ManipulatorState
import numpy as np
import tf

class SocialRobotInterface():
    def __init__(self):
        
        rospy.Subscriber("/social_robot/arm_states", JointState, self.callback_arm_state)
        rospy.Subscriber("/cur_Joint", JointState, self.callback_base_state)
    
        self.pub_state = rospy.Publisher("/social_robot/joint_states", JointState, queue_size=10)
        self.pub_robot_state = rospy.Publisher("/social_robot/states", ManipulatorState, queue_size=10)
        self.pub_odom = rospy.Publisher("/odom", Odometry, queue_size=50)

        self.robot_state = 'READY'
        self.arm_state = 'READY'
        self.base_state = []
        self.arm_joints = []
        self.prev_finger_pos = []
        self.prev_time = None
        self.goal_received = False
        self.cnt = 0
        self.yaw = 0

    def callback_arm_state(self, data):    
        self.arm_joints = data   
        arm_joints = self.estimateArmState(data) 
        self.updateArmState(arm_joints)
        return
    
    def callback_base_state(self, data):
        self.base_state = data
        return

    def estimateArmState(self, arm_joints):
        # get finger joint positions
        finger_pos = []
        current_time = rospy.get_time()
        fingers = ['RFinger_1','RFinger_2','RFinger_3','LFinger_1','LFinger_2','LFinger_3']

        for finger in fingers:
            pos = arm_joints.position[arm_joints.name.index(finger)]
            finger_pos.append(pos)
        if not self.prev_finger_pos:
            self.prev_finger_pos = finger_pos
        if not self.prev_time:
            self.prev_time = current_time

        diff_pos = np.subtract(finger_pos, self.prev_finger_pos)
        diff_time = current_time - self.prev_time

        if diff_time != 0: 
            finger_velocity =  diff_pos / diff_time   # rad/s array

            temp = list(arm_joints.velocity)
            for i, finger in enumerate(fingers):
                temp[arm_joints.name.index(finger)] = finger_velocity[i]
            arm_joints.velocity = tuple(temp)

        self.prev_time = current_time
        self.prev_finger_pos = finger_pos

        return arm_joints

    def updateArmState(self, arm_state):
        vel = 0.0
        for i in arm_state.velocity:
            if abs(i) > 0.03:
                vel+=1
        if vel>0 and (self.arm_state == 'READY' or self.arm_state == 'MOVING'):
            self.arm_state = 'MOVING'
        elif vel == 0 and (self.arm_state == 'MOVING' or self.arm_state == 'STOPPED'):
            self.arm_state = 'STOPPED'
            self.cnt += 1
        if self.cnt > 10 and self.arm_state == 'STOPPED':
            self.arm_state = 'READY'
            self.cnt = 0

        return

    def publishState(self):
        state = ManipulatorState()
        if self.arm_state == 'READY' :
            state.manipulator_moving_state = 'READY'
        elif self.arm_state == 'STOPPED' :
            state.manipulator_moving_state = 'STOPPED'
        elif self.arm_state == 'MOVING' :
            state.manipulator_moving_state = 'MOVING'
        self.pub_robot_state.publish(state)

    def run(self):
        
        rate = rospy.Rate(10) # 10hz
        while not rospy.is_shutdown():
            joint_state = JointState()
            joint_state.header.stamp = rospy.Time.now()

            # merge arm & mobile states
            arm_state = self.arm_joints
            base_state = self.base_state
            if(arm_state and base_state):
                joint_state.name = arm_state.name + base_state.name
                joint_state.position = arm_state.position + base_state.position
                joint_state.velocity = arm_state.velocity + (0,0,0,0)
                joint_state.effort = arm_state.effort + (0,0,0,0)
                self.pub_state.publish(joint_state)

            elif(arm_state and not base_state):
                joint_state.name = arm_state.name 
                joint_state.position = arm_state.position
                joint_state.velocity = arm_state.velocity
                joint_state.effort = arm_state.effort 
                self.pub_state.publish(joint_state)

            self.publishState()
            rate.sleep()


if __name__ == "__main__":
    rospy.init_node("social_robot_interface_python")
    social_if = SocialRobotInterface()
    social_if.run()

    
