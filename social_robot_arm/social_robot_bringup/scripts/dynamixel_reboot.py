#!/usr/bin/env python
from dynamixel import *               

# Protocol version
PROTOCOL_VERSION        = 2.0         
DXL_ID                  = 1                
BAUDRATE                = 1000000           
PORTLIST                = ['/dev/ttyUSB0', '/dev/ttyUSB1']

for port in PORTLIST:
    # try to connect 
    portHandler = PortHandler(port)
    packetHandler = PacketHandler(PROTOCOL_VERSION)

    # Open port
    if portHandler.openPort():
        print("Succeeded to open the %s port" %port)
    else:
        print("Failed to open %s port" %port)
        quit()

    # Set port baudrate
    if portHandler.setBaudRate(BAUDRATE):
        print("Succeeded to change the baudrate")
    else:
        print("Failed to change the baudrate")
        quit()

    # Get Dynamixel model number
    for id in range(20):
        dxl_model_number, dxl_comm_result, dxl_error = packetHandler.ping(portHandler, id)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % packetHandler.getRxPacketError(dxl_error))
        else:
            print("[ID:%03d] ping Succeeded. Dynamixel model number : %d" % (id, dxl_model_number))

            # Try reboot
            # Dynamixel LED will flicker while it reboots
            dxl_comm_result, dxl_error = packetHandler.reboot(portHandler, id)
            if dxl_comm_result != COMM_SUCCESS:
                print("%s" % packetHandler.getTxRxResult(dxl_comm_result))
            elif dxl_error != 0:
                print("%s" % packetHandler.getRxPacketError(dxl_error))

            print("[ID:%03d] reboot Succeeded\n" % id)

# Close port
portHandler.closePort()