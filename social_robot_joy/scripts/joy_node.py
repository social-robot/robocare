#!/usr/bin/env python
import rospy
from geometry_msgs.msg  import Twist
from sensor_msgs.msg import Joy, JointState
from std_msgs.msg import Empty, Bool

# XBOX ONE controller button mapping
BUTTON_A = 0
BUTTON_B = 1
BUTTON_X = 2
BUTTON_Y = 3 
BUTTON_LEFT_BUMPER = 4
BUTTON_RIGHT_BUMPER = 5
BUTTON_BACK = 6
BUTTON_START = 7
BUTTON_LEFT_STICK = 8
BUTTON_RIGHT_STICK = 9

# XBOX ONE controller axis mapping
# left = 1.0,   right = -1.0
# up = 1.0,     down = -1.0
# 
AXIS_LEFT_STICK_X = 0
AXIS_LEFT_STICK_Y = 1
AXIS_RIGHT_STICK_X = 3
AXIS_RIGHT_STICK_Y = 4
AXIS_LEFT_PAD_X = 6
AXIS_LEFT_PAD_Y = 7

LINEAR_SPEED_FACTOR = 0.25
ANGULAR_SPEED_FACTOR = 0.8


class RobotController():
    def __init__(self):
        rospy.init_node('social_robot_joy_controller', anonymous=True)
        self.pub_vel = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        self.pub_lidar = rospy.Publisher('/lidar_OnOff', Bool, queue_size=1)
        self.pub_reset = rospy.Publisher('/reset', Empty, queue_size=1)
        self.pub_pos = rospy.Publisher('/cmd_pos', JointState, queue_size=1)

        self.sub_joy = rospy.Subscriber('/joy',Joy, self._callback_joy)
        
        self.axes = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        self.buttons = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

        rospy.loginfo("[RobotController] Initialized.")
        self.rate = rospy.Rate(20)

    def __del__(self):
        self.stop

    def _callback_joy(self, data):
        self.buttons = data.buttons
        self.axes= data.axes

    def stop(self):
        rospy.loginfo("[RobotController] Shutdown.")
        twist = Twist()
        self.pub_vel.publish(twist)

    def control(self):
        while(not rospy.is_shutdown()): 
            self.button_control(self.buttons)   
            self.axis_control(self.buttons, self.axes)          
            self.rate.sleep()

    def axis_control(self, button, axis):
        twist = Twist()
        #if(button[BUTTON_RIGHT_BUMPER] and any(axis)):  # bumper and axis are pressed
        if(button[BUTTON_RIGHT_BUMPER]):  # bumper and axis are pressed

            # Copy state into twist message.
            twist.linear.x = axis[AXIS_LEFT_STICK_Y] * LINEAR_SPEED_FACTOR
            twist.linear.y = axis[AXIS_LEFT_STICK_X] * LINEAR_SPEED_FACTOR
            twist.linear.z = 0
            twist.angular.x = 0        
            twist.angular.y = 0        
            twist.angular.z = axis[AXIS_RIGHT_STICK_X] * ANGULAR_SPEED_FACTOR + axis[2] * ANGULAR_SPEED_FACTOR
            
            self.pub_vel.publish(twist)
        else:
            pass
            #self.pub_vel.publish(twist)

    def button_control(self, button):
        if(button[BUTTON_A]):
            #print('BUTTON_A is button.')
            # set robot init pose
            rospy.logwarn("Set Robot Init Pose.")
            jointstate = JointState()
            jointstate.header.stamp = rospy.Time.now()
            jointstate.name = ['Waist_Roll','Waist_Pitch','Head_Pitch','Head_Yaw']
            jointstate.position = [0, 0, 0, 0]
            jointstate.velocity = [0.1, 0.1, 0.1, 0.1]
            jointstate.effort = []
            self.pub_pos.publish(jointstate)
            pass
        if(button[BUTTON_B]):
            #print('BUTTON_B is button.')
            # set robot detect pose
            rospy.logwarn("Set Robot Detect Pose.")
            jointstate = JointState()
            jointstate.header.stamp = rospy.Time.now()
            jointstate.name = ['Waist_Roll','Waist_Pitch','Head_Pitch','Head_Yaw']
            jointstate.position = [0, 0.48, -0.3, 0]
            jointstate.velocity = [0.1, 0.1, 0.1, 0.1]
            jointstate.effort = []
            self.pub_pos.publish(jointstate)
            pass
        if(button[BUTTON_X]):
            #print('BUTTON_X is button.')
            pass
        if(button[BUTTON_Y]):
            #print('BUTTON_Y is button.')
            pass
        if(button[BUTTON_LEFT_BUMPER]):
            #print('BUTTON_LEFT_BUMPER is button.')
            pass
        if(button[BUTTON_RIGHT_BUMPER]):
            #print('BUTTON_RIGHT_BUMPER is button.')
            pass
        if(button[BUTTON_BACK]):
            #print('BUTTON_BACK is button.')
            # reset
            rospy.logwarn("Set Robot Reset.")
            self.pub_reset.publish(Empty())
            pass
        if(button[BUTTON_START]):
            #print('BUTTON_START is button.')
            # lidar on
            rospy.logwarn("Set Robot Lidar On.")
            cmd = Bool()
            cmd.data = True
            self.pub_lidar.publish(cmd)
            pass
        if(button[BUTTON_LEFT_STICK]):
            #print('BUTTON_LEFT_STICK is button.')
            pass
        if(button[BUTTON_RIGHT_STICK]):
            #print('BUTTON_RIGHT_STICK is button.')
            pass
                

if __name__ == '__main__':
    controller = RobotController()
    controller.control()
    controller.stop()
