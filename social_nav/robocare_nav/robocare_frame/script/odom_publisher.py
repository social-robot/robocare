#!/usr/bin/env python
import rospy
import numpy
import sys, select, os
from std_msgs.msg import String
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from math import cos, sin, tan, pi
import tf
Vx,Vy,W =0.0,0.0,0.0
x,y,heading = 0.0,0.0,0.0
previousTime = 0
def control_input(msg):
    global Vx,Vy,W
    Vx = msg.linear.x 
    Vy = msg.linear.y
    W = msg.angular.z   

def odom_pub():
    global Vx,Vy,W,x,y,heading
    rospy.init_node('odom_pub_node', anonymous=True)
    sub = rospy.Subscriber('cmd_vel', Twist, control_input)
    rate = rospy.Rate(100) 
    odom_publisher = rospy.Publisher('odom', Odometry,queue_size=1)
    previousTime = rospy.Time.now()
    while not rospy.is_shutdown():        
        # 
        delta_time = (rospy.Time.now() - previousTime).to_sec()
        delta_x = (Vx * cos(W) - Vy * sin(W))*delta_time
        delta_y = (Vx * sin(W) + Vy * cos(W))*delta_time
        delta_th = W*delta_time
        # 
        x = x + delta_x
        y = y + delta_y
        heading = heading + delta_th

        #
        pubMsg = Odometry()
        pubMsg.header.stamp = rospy.Time.now()
        pubMsg.header.frame_id = '/odom'
        pubMsg.pose.pose.position.x = x
        pubMsg.pose.pose.position.y = y
        quat = tf.transformations.quaternion_from_euler(0.0,0.0,heading)
        pubMsg.pose.pose.orientation.x = quat[0]
        pubMsg.pose.pose.orientation.y = quat[1]
        pubMsg.pose.pose.orientation.z = quat[2]
        pubMsg.pose.pose.orientation.w = quat[3]
        odom_publisher.publish(pubMsg)
        previousTime = rospy.Time.now()
        rate.sleep()

if __name__ == '__main__':
    try:
        odom_pub()
    except rospy.ROSInterruptException:
        pass