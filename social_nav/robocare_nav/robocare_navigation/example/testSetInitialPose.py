#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseWithCovariance



def set_initial_pose():
    pub = rospy.Publisher("/initialpose", PoseWithCovarianceStamped, queue_size=1)
    rate = rospy.Rate(10)
    while(not rospy.is_shutdown()):
        req = PoseWithCovarianceStamped()
        req.header.stamp = rospy.Time.now()
        req.header.frame_id = "map"
        req.pose.pose.position.x = 1.4236741066
        req.pose.pose.position.y = -0.0788351297379
        req.pose.pose.orientation.x = 0
        req.pose.pose.orientation.y = 0
        req.pose.pose.orientation.z = -0.674296986123
        req.pose.pose.orientation.w = 0.738460272801
        rate.sleep()
        pub.publish(req)

# If the python node is executed as main process (sourced directly)
if __name__ == '__main__':

    rospy.init_node('movebase_client_py')
    result = set_initial_pose()

